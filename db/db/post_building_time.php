<?PHP
// add connected community readings

require "conn_uqconnection.php";

date_default_timezone_set("Australia/Brisbane");

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
	$data = explode(",", $_POST["data"]);
	//echo "buildingno: " . $data[0];
	
	$buildingnumber = $data[0];
	$day = $data[1];
	$hour = $data[2];
	$minblock = $data[3];
	
	//echo $buildingnumber . "is the building number ";
	switch ($day)
	{
		case 1:
			$date='6/10/18';
			break;
		case 2:
			$date='7/10/18';
			break;
		case 3:
			$date='8/10/18';
			break;
		default:
			$date='7/10/18';
			break;
	}
	
	$qrystmt2 = $conn->prepare("SELECT people FROM wifconnsummary WHERE buildingnumber=:bn AND date=:day AND hour=:hr AND min10=:mb");
	$qrystmt2->bindValue(':bn', $buildingnumber);
	$qrystmt2->bindValue(':day', $date);
	$qrystmt2->bindValue(':hr', $hour);
	$qrystmt2->bindValue(':mb', $minblock);
	$qrystmt2->execute();

	//$result = $qrystmt2->fetchAll();
	
	//echo sizeof($result);
	//$result_json = json_encode($result);
	//echo $result_json;
	
	$result = $qrystmt2->fetchColumn();
	echo $result;
	
}
else
{
	echo "Error 002 - wrong request type";	
}

$conn = null;

?>